#include "lfscv.h"
#include "lfsinsim.h"
#include "InsimPackets/insiminit.h"
#include "InsimPackets/insimtiny.h"

#include "lfs_wrapped.h"
#include <string>

#include <QDateTime>

#include <algorithm>

lfscv::lfscv(QObject *parent)
    : lfs_insim_initialized (false),
      cancel_vote_enabled (true) {
}

lfscv& lfscv::setPassword (std::string password) {
    this->password = password;
    return *this;
}

lfscv& lfscv::setAddress (std::string address) {
    this->address = address;
    return *this;
}

lfscv& lfscv::setPort (unsigned port){
    this->port = port;
    return *this;
}

void lfscv::dataReceived() {
    char buf[256];

    while (true) {
        unsigned bytes = sock.bytesAvailable();
        sock.read((char*) buf, 1);
        unsigned char len = buf[0];
        if (len > bytes) {
            sock.ungetChar(buf[0]);
            qDebug() << "skipping unfinished packet";
            break;
        }
        sock.read(buf + 1, len - 1);
        char* cur = buf;

        qDebug() << QString::fromUtf8(get_packet_name(cur, true).c_str()) << "(" << unsigned(len) << " bytes )";

        // keep alive
        if (cur[1] == 3 && cur[3] == 0) {
            InsimTiny packet = InsimTiny(TINY_NONE);
            this->sock.write(packet.getPacket(), 4);
        }
        else {
            this->gotPacket(cur);
        }
        if (len == bytes) {
            break;
        }
    }
}


#define PTYPE_AI 2
#define PTYPE_REMOTE 4
void lfscv::gotPacket (char* packet) {
    unsigned char type = packet[1];
    switch (type) {
    case ISP_VER: // we get this after we connect
    {
        InsimTiny sst = InsimTiny(TINY_SST, 1);
        this->sock.write(sst.getPacket(), 4);

        InsimTiny rst = InsimTiny(TINY_RST, 1);
        this->sock.write(rst.getPacket(), 4);

        InsimTiny ncn = InsimTiny(TINY_NCN, 1);
        this->sock.write(ncn.getPacket(), 4);
    }
    break;
    case ISP_VTN:
    {
        IS_VTN* vtn = (IS_VTN*) packet;

        bool is_admin = std::find(admin_ids.begin(),
                                  admin_ids.end(),
                                  vtn->UCID)
            != admin_ids.end();


        if (cancel_vote_enabled && (!is_admin)) {
            if (vtn->Action != VOTE_NONE) {
                InsimTiny cv = InsimTiny(TINY_VTC);
                this->sock.write(cv.getPacket(), 4);
            }
        }
    }
    break;
    case ISP_MSO:
    {
        IS_MSO* mso = (IS_MSO*) packet;
        if(std::find(admin_ids.begin(),
                     admin_ids.end(),
                     mso->UCID)
           != admin_ids.end()) {
            std::string mess (mso->Msg + mso->TextStart);
            if (mess == "@cvon") {
                cancel_vote_enabled = true;
            }
            if (mess == "@cvoff") {
                cancel_vote_enabled = false;
            }
        }
    }
    break;
    case ISP_NCN:
    {
        IS_NCN* ncn = (IS_NCN*) packet;
        if (ncn->Admin) {
            this->admin_ids.push_back(ncn->UCID);
            qDebug() << "admin connected";
        }
    }
    break;
    case ISP_CNL:
    {
        IS_CNL* cnl = (IS_CNL*) packet;
        if(std::find(admin_ids.begin(),
                     admin_ids.end(),
                     cnl->UCID)
           != admin_ids.end()) {
            admin_ids.erase(std::remove(admin_ids.begin(),
                                        admin_ids.end(),
                                        cnl->UCID),
                            admin_ids.end());
        }
    }
    break;
    }
}

void lfscv::stateChanged (QAbstractSocket::SocketState state) {
    //qDebug() << "host state changed to" << state;
    if (state == QAbstractSocket::ConnectedState) {
        InsimInit packet = InsimInit ("lfscv", this->password.c_str())
                .set_get_version(true);
        this->sock.write(packet.getPacket(), 44);
    }
}

void lfscv::errorOccured (QAbstractSocket::SocketError err) {
    qDebug() << "ERROR: " << err;
    switch (err) {
    case QAbstractSocket::RemoteHostClosedError:
        qDebug() << "Probably your LFS password is not configured properly here.";
        qDebug() << "To check your password enter LFS <Multiplayer> - <Start new host> - <OK> and see the <Admin password> field.\n";
        break;
    default:
        qDebug() << "Could not connect to LFS. Is LFS started? Is port opened?";
        qDebug() << "Port setting is:" << this->port;
        break;
    }
}

void lfscv::run (){
    // Do processing here
    this->sock.connectToHost(this->address.c_str(), this->port);
    connect(&this->sock, SIGNAL(readyRead()), this, SLOT(dataReceived()));
    connect(&this->sock, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(stateChanged(QAbstractSocket::SocketState)));
    connect(&this->sock, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(errorOccured(QAbstractSocket::SocketError)));

    //emit finished();
}
