#include <QCoreApplication>
#include <QTimer>

#include "lfscv.h"

#include "MKeyFile.hpp"
#include <string>
#include <map>
#include <sstream>

template <typename T>
inline T fromString (const std::string &str) {
    std::stringstream ss_val(str);
    T ret_val;
    ss_val >> ret_val;
    return ret_val;
}

typedef std::map<std::string, std::string> SettingsMap;

template <typename T>
T
get_config_value (SettingsMap& settings,
                  const std::string& option_name,
                  const T& default_value) {
    if (settings.count(option_name)) {
        return fromString<T>(settings[option_name]);
    }
    return default_value;
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    SettingsMap settings;
    try {
        MKeyFile f("config.ini");
        MSectionsIterator sec_iter = f.getSectionsIterator();
        while (sec_iter.isElement()) {
            MSettingsIterator set_iter = sec_iter.getSettingsIterator();
            const std::string sec_name = sec_iter.getName();
            const std::string prefix =
                sec_name.empty() ? "" : sec_name + ".";
            while (set_iter.isElement()) {
                settings[prefix + set_iter.getName()] =
                    set_iter.getValue();
                set_iter.peekNext();
            }
            sec_iter.peekNext();
        }
    }
    catch (MKeyFile::file_not_found) {
        qDebug() << "config file not present: " << "config.ini";
    }

    lfscv s;
    s.setPassword(get_config_value<std::string>(settings, "password", ""))
     .setPort(get_config_value<unsigned>(settings, "port", 29999))
     .setAddress(get_config_value<std::string>(settings, "host", "localhost"));

    QObject::connect(&s, SIGNAL(finished()), &a, SLOT(quit()));

    // This will run the task from the application event loop.
    QTimer::singleShot(0, &s, SLOT(run()));
    return a.exec();
}
