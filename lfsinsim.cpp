#include "lfsinsim.h"
#include <cstring>

#include <vector>
#include <utility>

#include <sstream>

#include "lfs_wrapped.h"

std::string get_packet_name (char *ptr, bool append_description) {
   static struct S {
       std::vector<std::pair<std::string, std::string> > v;
       std::vector<std::pair<std::string, std::string> > vt;
       std::vector<std::pair<std::string, std::string> > vs;

       S() {
            v.push_back(std::pair<std::string, std::string>("ISP_NONE", "not used"));
            v.push_back(std::pair<std::string, std::string>("ISP_ISI", "insim initialise"));
            v.push_back(std::pair<std::string, std::string>("ISP_VER", "version info"));
            v.push_back(std::pair<std::string, std::string>("ISP_TINY", "multi purpose"));
            v.push_back(std::pair<std::string, std::string>("ISP_SMALL", "multi purpose"));
            v.push_back(std::pair<std::string, std::string>("ISP_STA", "state info"));
            v.push_back(std::pair<std::string, std::string>("ISP_SCH", "single character"));
            v.push_back(std::pair<std::string, std::string>("ISP_SFP", "state flags pack"));
            v.push_back(std::pair<std::string, std::string>("ISP_SCC", "set car camera"));
            v.push_back(std::pair<std::string, std::string>("ISP_CPP", "cam pos pack"));
            v.push_back(std::pair<std::string, std::string>("ISP_ISM", "start multiplayer"));
            v.push_back(std::pair<std::string, std::string>("ISP_MSO", "message out"));
            v.push_back(std::pair<std::string, std::string>("ISP_III", "hidden /i message"));
            v.push_back(std::pair<std::string, std::string>("ISP_MST", "type message or /command"));
            v.push_back(std::pair<std::string, std::string>("ISP_MTC", "message to a connection"));
            v.push_back(std::pair<std::string, std::string>("ISP_MOD", "set screen mode"));
            v.push_back(std::pair<std::string, std::string>("ISP_VTN", "vote notification"));
            v.push_back(std::pair<std::string, std::string>("ISP_RST", "race start"));
            v.push_back(std::pair<std::string, std::string>("ISP_NCN", "new connection"));
            v.push_back(std::pair<std::string, std::string>("ISP_CNL", "connection left"));
            v.push_back(std::pair<std::string, std::string>("ISP_CPR", "connection renamed"));
            v.push_back(std::pair<std::string, std::string>("ISP_NPL", "new player (joined race)"));
            v.push_back(std::pair<std::string, std::string>("ISP_PLP", "player pit (keeps slot in race)"));
            v.push_back(std::pair<std::string, std::string>("ISP_PLL", "player leave (spectate - loses slot)"));
            v.push_back(std::pair<std::string, std::string>("ISP_LAP", "lap time"));
            v.push_back(std::pair<std::string, std::string>("ISP_SPX", "split x time"));
            v.push_back(std::pair<std::string, std::string>("ISP_PIT", "pit stop start"));
            v.push_back(std::pair<std::string, std::string>("ISP_PSF", "pit stop finish"));
            v.push_back(std::pair<std::string, std::string>("ISP_PLA", "pit lane enter / leave"));
            v.push_back(std::pair<std::string, std::string>("ISP_CCH", "camera changed"));
            v.push_back(std::pair<std::string, std::string>("ISP_PEN", "penalty given or cleared"));
            v.push_back(std::pair<std::string, std::string>("ISP_TOC", "take over car"));
            v.push_back(std::pair<std::string, std::string>("ISP_FLG", "flag (yellow or blue)"));
            v.push_back(std::pair<std::string, std::string>("ISP_PFL", "player flags (help flags)"));
            v.push_back(std::pair<std::string, std::string>("ISP_FIN", "finished race"));
            v.push_back(std::pair<std::string, std::string>("ISP_RES", "result confirmed"));
            v.push_back(std::pair<std::string, std::string>("ISP_REO", "reorder (info or instruction)"));
            v.push_back(std::pair<std::string, std::string>("ISP_NLP", "node and lap packet"));
            v.push_back(std::pair<std::string, std::string>("ISP_MCI", "multi car info"));
            v.push_back(std::pair<std::string, std::string>("ISP_MSX", "type message"));
            v.push_back(std::pair<std::string, std::string>("ISP_MSL", "message to local computer"));
            v.push_back(std::pair<std::string, std::string>("ISP_CRS", "car reset"));
            v.push_back(std::pair<std::string, std::string>("ISP_BFN", "delete buttons / receive button requests"));
            v.push_back(std::pair<std::string, std::string>("ISP_AXI", "autocross layout information"));
            v.push_back(std::pair<std::string, std::string>("ISP_AXO", "hit an autocross object"));
            v.push_back(std::pair<std::string, std::string>("ISP_BTN", "show a button on local or remote screen"));
            v.push_back(std::pair<std::string, std::string>("ISP_BTC", "sent when a user clicks a button"));
            v.push_back(std::pair<std::string, std::string>("ISP_BTT", "sent after typing into a button"));
            v.push_back(std::pair<std::string, std::string>("ISP_RIP", "replay information packet"));
            v.push_back(std::pair<std::string, std::string>("ISP_SSH", "screenshot"));
            v.push_back(std::pair<std::string, std::string>("ISP_CON", "contact between cars (collision report)"));
            v.push_back(std::pair<std::string, std::string>("ISP_OBH", "contact car + object (collision report)"));
            v.push_back(std::pair<std::string, std::string>("ISP_HLV", "report incidents that would violate HLVC"));
            v.push_back(std::pair<std::string, std::string>("ISP_PLC", "player cars"));
            v.push_back(std::pair<std::string, std::string>("ISP_AXM", "autocross multiple objects"));
            v.push_back(std::pair<std::string, std::string>("ISP_ACR", "admin command report"));

            vt.push_back(std::pair<std::string, std::string>("TINY_NONE", "keep alive (see \"maintaining the connection\")"));
            vt.push_back(std::pair<std::string, std::string>("TINY_VER", "get version"));
            vt.push_back(std::pair<std::string, std::string>("TINY_CLOSE", "close insim"));
            vt.push_back(std::pair<std::string, std::string>("TINY_PING", "external progam requesting a reply"));
            vt.push_back(std::pair<std::string, std::string>("TINY_REPLY", "reply to a ping request"));
            vt.push_back(std::pair<std::string, std::string>("TINY_VTC", "game vote cancel (info or request)"));
            vt.push_back(std::pair<std::string, std::string>("TINY_SCP", "send camera pos"));
            vt.push_back(std::pair<std::string, std::string>("TINY_SST", "send state info"));
            vt.push_back(std::pair<std::string, std::string>("TINY_GTH", "get time in hundredths (i.e. SMALL_RTP)"));
            vt.push_back(std::pair<std::string, std::string>("TINY_MPE", "multi player end"));
            vt.push_back(std::pair<std::string, std::string>("TINY_ISM", "get multiplayer info (i.e. ISP_ISM)"));
            vt.push_back(std::pair<std::string, std::string>("TINY_REN", "race end (return to race setup screen)"));
            vt.push_back(std::pair<std::string, std::string>("TINY_CLR", "all players cleared from race"));
            vt.push_back(std::pair<std::string, std::string>("TINY_NCN", "get all connections"));
            vt.push_back(std::pair<std::string, std::string>("TINY_NPL", "get all players"));
            vt.push_back(std::pair<std::string, std::string>("TINY_RES", "get all results"));
            vt.push_back(std::pair<std::string, std::string>("TINY_NLP", "send an IS_NLP"));
            vt.push_back(std::pair<std::string, std::string>("TINY_MCI", "send an IS_MCI"));
            vt.push_back(std::pair<std::string, std::string>("TINY_REO", "send an IS_REO"));
            vt.push_back(std::pair<std::string, std::string>("TINY_RST", "send an IS_RST"));
            vt.push_back(std::pair<std::string, std::string>("TINY_AXI", "send an IS_AXI - AutoX Info"));
            vt.push_back(std::pair<std::string, std::string>("TINY_AXC", "autocross cleared"));
            vt.push_back(std::pair<std::string, std::string>("TINY_RIP", "send an IS_RIP - Replay Information Packet"));

             vs.push_back(std::pair<std::string, std::string>("SMALL_NONE", "not used"));
             vs.push_back(std::pair<std::string, std::string>("SMALL_SSP", "start sending positions"));
             vs.push_back(std::pair<std::string, std::string>("SMALL_SSG", "start sending gauges"));
             vs.push_back(std::pair<std::string, std::string>("SMALL_VTA", "vote action"));
             vs.push_back(std::pair<std::string, std::string>("SMALL_TMS", "time stop"));
             vs.push_back(std::pair<std::string, std::string>("SMALL_STP", "time step"));
             vs.push_back(std::pair<std::string, std::string>("SMALL_RTP", "race time packet (reply to GTH)"));
             vs.push_back(std::pair<std::string, std::string>("SMALL_NLI", "set node lap interval"));
          }
    } stor;

   std::string description = stor.v[ptr[1]].second;
   if (ptr[1] == 3) {
       description = stor.vt[ptr[3]].second;
   }
   else if (ptr[1] == 4) {
       description = stor.vs[ptr[3]].second;
   }
   return stor.v[ptr[1]].first + (append_description ? ": " + description : "");

};


std::string pretty_print_lap_time (unsigned ltime) {
    short minutes = ltime / 1000 / 60;
    short seconds = (ltime - (minutes * 1000 * 60)) / 1000;
    short milliseconds = (ltime - (minutes * 1000 * 60) - seconds * 1000);

    std::ostringstream buffer;


    buffer << minutes << ":"; // << ":" << (seconds < 10 ? std::string("0") + std::string(seconds) : std::srting(seconds)) << "." << (milliseconds < 10 ? std::string("0") + std::string(milliseconds) : std::string(milliseconds)) << std::endl;
    seconds < 10 && buffer << 0;
    buffer << seconds << ".";
    milliseconds < 10 && buffer << 0;
    milliseconds < 100 && buffer << 0;
    buffer << milliseconds;
    return buffer.str();
}
