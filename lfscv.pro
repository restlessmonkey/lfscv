#-------------------------------------------------
#
# Project created by QtCreator 2014-09-20T11:59:50
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = lfscv
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += static

TEMPLATE = app


SOURCES += main.cpp \
    lfsinsim.cpp \
    InsimPackets/insiminit.cpp \
    InsimPackets/insimtiny.cpp \
    MKeyFile.cpp \
    lfscv.cpp

HEADERS += \
    lfsinsim.h \
    InsimPackets/insiminit.h \
    InsimPackets/insimtiny.h \
    lfs_wrapped.h \
    original_lfs_insim.h \
    MKeyFile.hpp \
    StringUtils.hpp \
    lfscv.h

OTHER_FILES += \
    README.DEVELOPER
