#ifndef lfscv_H
#define lfscv_H

#include <QObject>
#include <QTcpSocket>

#include <fstream>

#include <string>
#include <vector>

class lfscv : public QObject
{
    Q_OBJECT
public:
    lfscv (QObject *parent = 0);
    lfscv& setAddress (std::string password);
    lfscv& setPassword (std::string password);
    lfscv& setPort (unsigned port);

public slots:
    void dataReceived ();

    void stateChanged (QAbstractSocket::SocketState state);

    void errorOccured (QAbstractSocket::SocketError err);

    void run();


signals:
    void finished();

private:
    void gotPacket(char* packet);
    QTcpSocket sock;
    bool lfs_insim_initialized;

    std::string address;
    std::string password;
    std::vector<unsigned> admin_ids;
    unsigned port;

    bool cancel_vote_enabled;
};

#endif // lfscv_H
