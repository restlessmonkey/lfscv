#ifndef LFSINSIM_H
#define LFSINSIM_H

#include <string>

//longest packet length is 252 (IS_BTN)?

std::string get_packet_name (char* packet_ptr, bool append_description = false);
std::string pretty_print_lap_time (unsigned ltime);
#endif // LFSINSIM_H

