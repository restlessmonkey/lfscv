#include "insiminit.h"
#include "lfs_wrapped.h"
#include <cstring>

InsimInit::InsimInit (const std::string &program_name,
                      const std::string &password) {
    this->packet = (IS_ISI*) &this->packet_storage;
    memset(this->packet, 0, 44);
    this->packet->Size = 44;
    this->packet->Type = ISP_ISI;

    strcpy((char*) this->packet->Admin, password.c_str());
    strcpy((char*) this->packet->IName, program_name.c_str());
}

char* InsimInit::getPacket() {
    return (char*) this->packet;
}

InsimInit& InsimInit::set_get_version (bool s) {
    this->packet->ReqI = s;
    return *this;
}

InsimInit& InsimInit::set_udp_port (unsigned short s) {
    this->packet->UDPPort = s;
    return *this;
}

void set_flag (unsigned short &flags, unsigned short flag, bool s) {
    flags = s ? flags | flag : flags & ~flag;
}

InsimInit& InsimInit::set_local (bool s) {
    set_flag(this->packet->Flags, ISF_LOCAL, s);
    return *this;
}

InsimInit& InsimInit::set_keep_colors (bool s) {
    set_flag(this->packet->Flags, ISF_MSO_COLS, s);
    return *this;
}

InsimInit& InsimInit::set_nlp (bool s) {
    set_flag(this->packet->Flags, ISF_NLP, s);
    return *this;
}

InsimInit& InsimInit::set_mci (bool s) {
    set_flag(this->packet->Flags, ISF_MCI, s);
    return *this;
}

InsimInit& InsimInit::set_con (bool s) {
    set_flag(this->packet->Flags, ISF_CON, s);
    return *this;
}

InsimInit& InsimInit::set_obh (bool s) {
    set_flag(this->packet->Flags, ISF_OBH, s);
    return *this;
}

InsimInit& InsimInit::set_hlv (bool s) {
    set_flag(this->packet->Flags, ISF_HLV, s);
    return *this;
}

InsimInit& InsimInit::set_axm_load (bool s) {
    set_flag(this->packet->Flags, ISF_AXM_LOAD, s);
    return *this;
}

InsimInit& InsimInit::set_axm_edit (bool s) {
    set_flag(this->packet->Flags, ISF_AXM_EDIT, s);
    return *this;
}

InsimInit& InsimInit::set_host_prefix (char s) {
    this->packet->Prefix = s;
    return *this;
}

InsimInit& InsimInit::set_interval (unsigned short s) {
    this->packet->Interval = s;
    return *this;
}
