#ifndef INSIMTINY_H
#define INSIMTINY_H

struct IS_TINY;

class InsimTiny
{
public:
    InsimTiny(char type, char request_id = 0);
    char* getPacket ();
private:
    char packet_storage[4];
    IS_TINY* packet; // TODO: change to char[n]
};

#endif // INSIMTINY_H
