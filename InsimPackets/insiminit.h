#ifndef INSIMINIT_H
#define INSIMINIT_H

#include <string>

struct IS_ISI;

class InsimInit {
public:
    InsimInit (const std::string &program_name,
               const std::string &password = "");
    char* getPacket ();

    InsimInit& set_get_version (bool s);
    InsimInit& set_udp_port (unsigned short s);
    InsimInit& set_local (bool s);
    InsimInit& set_keep_colors (bool s);
    InsimInit& set_nlp (bool s);
    InsimInit& set_mci (bool s);
    InsimInit& set_con (bool s);
    InsimInit& set_obh (bool s);
    InsimInit& set_hlv (bool s);
    InsimInit& set_axm_load (bool s);
    InsimInit& set_axm_edit (bool s);
    InsimInit& set_host_prefix (char s);
    InsimInit& set_interval (unsigned short s);

    // TODO getters
    // TODO unpacking from IS_ISI struct

private:
    char packet_storage[44];
    IS_ISI *packet;
};

#endif // INSIMINIT_H
