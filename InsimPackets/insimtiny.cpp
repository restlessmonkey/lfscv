#include "insimtiny.h"
#include "lfs_wrapped.h"

InsimTiny::InsimTiny(char type, char request_id){
    this->packet = (IS_TINY*) this->packet_storage;
    this->packet->Size = 4;
    this->packet->Type = ISP_TINY;
    this->packet->ReqI = request_id;
    this->packet->SubT = type;
}

char* InsimTiny::getPacket() {
    return this->packet_storage;
}

